# ASCII_Art
- pip install pygame
- pip install Pillow

### Arguments
- *FRAMES_FOLDER*   :string Folder containing frames
- *ASCII_FILE*      :string Ascii text file name
- *MP3*             :string Mp3 file name to play
- *FPS*             :float  Console animation fps

Open windows console in fullscreen

### Generate
python app.py -e *FRAMES_FOLDER* *ASCII_FILE*

### Play
python app.py -p *ASCII_FILE* *MP3* *FPS*

### Youtube link
https://www.youtube.com/watch?v=FtutLA63Cp8

### Google Drive Assets link
https://drive.google.com/open?id=1PcqSGpthaR9FAnjC01RFd5KUoAyt-Y-n
