from PIL import Image
from PIL import ImageOps
from pygame import mixer
from pygame import time
import math
import os
import argparse


ASCII_CHARS_FULL = '$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`\'. '
ASCII_CHARS_MINIMAL = '@%#*+=-:. '
ASCII_CHARS = ASCII_CHARS_MINIMAL


def lerp(a, b, t):
    _min = min(a, b)
    _max = max(a, b)
    l = abs(_max - _min)
    assert t >= 0.0 and t <= 1.0
    if l == 0.0:
        return (_min + _max) / 2.0
    return l * t + _min


def inv_lerp(a, b, n):
    _min = min(a, b)
    _max = max(a, b)
    l = abs(_max - _min)
    assert n >= _min and n <= _max
    if l == 0.0:
        return 0.5
    return (n - _min) / l


def resize(image, new_width=100):
    width, height = image.size
    new_height = round(new_width * height / width)
    return image.resize((new_width, new_height))


def to_greyscale(image):
    return image.convert('L')


def pixel_to_ascii_char(pixel):
    return round(lerp(0, len(ASCII_CHARS) - 1, inv_lerp(0, 255, pixel)))


def pixels_to_ascii(image):
    pixels = image.getdata()
    ascii_str = ''
    for pixel in pixels:
        ascii_str += ASCII_CHARS[pixel_to_ascii_char(pixel)]
    return ascii_str


def split_to_lines(ascii_str, width):
    ascii_str_len = len(ascii_str)
    ascii_img = ''
    for i in range(0, ascii_str_len, width):
        ascii_img += ascii_str[i:i + width] + '\n'
    return ascii_img


def frames_to_ascii(folder_name, out_file):
    frame_names = []
    for file_name in os.listdir(folder_name):
        if os.path.isfile(os.path.join(folder_name, file_name)):
            frame_names.append(folder_name + '/' + file_name)

    ascii_vid = ''
    for frame_name in frame_names:
        with Image.open(frame_name) as frame:
            # frame = ImageOps.exif_transpose(frame)
            resized_image = resize(frame, 100)
            greyscale_image = to_greyscale(resized_image)
            ascii_str = pixels_to_ascii(greyscale_image)
            ascii_img = split_to_lines(ascii_str, greyscale_image.width)
            ascii_vid += ascii_img + 'FRAME_BREAK\n'

    with open(out_file, 'w') as f:
        f.write(ascii_vid)


def play_ascii(ascii_file, mp3, fps):
    with open(ascii_file, 'r') as f:
        frames = f.read().split('FRAME_BREAK\n')
        if mp3 is not None:
            mixer.init()
            mixer.music.load(mp3)
            mixer.music.play()
        clock = time.Clock()
        for frame in frames:
            print(frame, end='')
            clock.tick(fps)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--generate', nargs=2, default=None, type=str)
    parser.add_argument('-p', '--play', nargs=3, default=None, type=str)
    args = parser.parse_args()

    if args.generate is not None:
        frames_to_ascii(args.generate[0], args.generate[1])
    elif args.play is not None:
        play_ascii(args.play[0], args.play[1], float(args.play[2]))
    else:
        print("Argument Error")
